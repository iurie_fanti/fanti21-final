# Individual Project
A simple game of Briscola that can be played against an AI, but its main purpose its observing the different AI algorithms play amongst themselves and evaluating the gap in ability.

# How to run
Java must be installed on the machine. On the terminal input the command: java -jar briscola.jar
