package main;
import java.util.logging.*;

public class LogFormat extends Formatter {
    @Override
    public String format(LogRecord record) {
        StringBuffer log = new StringBuffer();
        log.append(record.getMessage());
        log.append(System.getProperty("line.separator"));
        return log.toString();
    }
}