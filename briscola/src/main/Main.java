package main;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.*;

public class Main {
    /* To communicate with the player during the game, we use a Logger to keep track of and output turns.
    *  This function handles the Logger format shown on the terminal. */
    private static void globalLoggingConfig () {
        Logger rootLogger = LogManager.getLogManager().getLogger("");
        for (Handler h : rootLogger.getHandlers()) {
            rootLogger.removeHandler(h);
        }

        StreamHandler sh = new StreamHandler(System.out, new LogFormat()) {
            @Override
            public synchronized void publish(final LogRecord record) {
                super.publish(record);
                flush();
            }
        };

        rootLogger.addHandler(sh);
        rootLogger.setLevel(Level.INFO);

        for (Handler h : rootLogger.getHandlers()) {
            h.setLevel(Level.INFO);
        }
    }


    /* The main function prompts the user to pick a GameMode and the number of rounds, before starting the Game. */
    public static void main(String[] args) {
        globalLoggingConfig(); // Initialises the Logger's configuration.

        System.out.println("- Available Briscola Modes - \n" +
                "0. Human vs AI rule-based \n" +
                "1. Human vs AI minmax \n" +
                "2. AI minmax vs AI rule-based \n" +
                "3. AI minmax vs AI random choice \n\n" +
                "Enter the number of the mode you want to play: ");


        Scanner scanner = new Scanner(System.in);
        String input;

        int gameToPlay = -1;

        do {
            input = scanner.nextLine();
            // If the input is not a single digit number, the user is prompted again.
            if (!input.matches("^\\d$")) {
                System.out.println("Not a valid choice. Enter a number from 0 to 3: ");
                continue;
            }
            gameToPlay = Integer.parseInt(input);
        } while (gameToPlay < 0 || gameToPlay > 3); // Loop is repeated until input is within allowed ranges.


        System.out.println("Enter the number of matches to play: ");

        int numberOfGames = -1;

        do {
            input = scanner.nextLine();
            // If the input is not a number between 1 and 7 digits, the user is prompted again.
            if (!input.matches("^\\d{1,7}$")) {
                System.out.println("Not a valid choice. Enter a number from 1 to 1000000: ");
                continue;
            }
            numberOfGames = Integer.parseInt(input);
        } while (numberOfGames < 1 || numberOfGames > 1000000); // Loop is repeated until input is within allowed ranges.


        // The "main" function of the Game, what makes the turns run until the end of play.
        briscola.Engine briscola = new briscola.Engine();

        // An ArrayList we will use for our AI-minmax search inside the game.
        List<Integer> searchParameters = new ArrayList<>();

        // Game results to be displayed at the end of all matches.
        int player0Victories = 0, player1Victories = 0, draws = 0, winner = -2;

        // This loop is repeated as many times as the rounds of plays decided by the user.
        for (int i = 0; i < numberOfGames; i++) {
            /* Depending on the GameMode we chose, we will have a different instance of Briscola.
            *  At the end of the Engine's run the winner of the match is returned. */
            switch (gameToPlay) {
                case 0:
                    winner = briscola.playNewGame(0, GameMode.HUMAN__VS__AI_RULE, searchParameters);
                    break;

                case 1:
                    searchParameters.add(7);  // Depth.
                    searchParameters.add(200);  // Random deals to search.
                    winner = briscola.playNewGame(0, GameMode.HUMAN__VS__AI_MINMAX, searchParameters);
                    break;

                case 2:
                    searchParameters.add(5);  // Depth.
                    searchParameters.add(100);  // Random deals to search.
                    winner = briscola.playNewGame(0, GameMode.AI_MINMAX__VS__AI_RULE, searchParameters);
                    break;

                case 3:
                    searchParameters.add(5);  // Depth.
                    searchParameters.add(100);  // Random deals to search.
                    winner = briscola.playNewGame(0, GameMode.AI_MINMAX__VS__AI_RANDOM, searchParameters);
                    break;
            }

            System.out.println("Game n. " + (i+1) + " ended with winner " + winner);

            // Update to the total ranking.
            switch (winner) {
                case 0:
                    player0Victories++;
                    break;
                case 1:
                    player1Victories++;
                    break;
                default:
                    draws++;
                    break;
            }

        }

        // Output of the final results from all the matches played.
        System.out.println("\nPlayer 0 won: " + player0Victories + " games (ratio  " + (player0Victories * 100.0) / numberOfGames + "%)" +
                "\nPlayer 1 won: " + player1Victories + " games (ratio " + (player1Victories * 100.0) / numberOfGames + "%)" +
                "\nDraws: " + draws + " games (ratio " + draws + "%)");
    }
}
