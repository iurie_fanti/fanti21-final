package main;

public enum GameMode {
    HUMAN__VS__AI_MINMAX,
    HUMAN__VS__AI_RULE,
    AI_MINMAX__VS__AI_RULE,
    AI_MINMAX__VS__AI_RANDOM
}

