package deck;
import java.util.*;

/* This class represents the Deck used to play the game. */
public class Deck {
    // The current number of cards in the Deck.
    private Integer cardsLeft;

    // The full list of all cards contained in the Deck.
    private List<Card> cards = new ArrayList<Card>();

    // The constructor instances a Card for every possible Rank of every possible Suit.
    public Deck () {
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                cards.add(new Card(rank, suit));
            }
        }
        cardsLeft = cards.size();
    }

    public Deck (Hand oldDeck) {
        for (int i = 0; i < oldDeck.getHand().size(); i++) {
            cards.add(oldDeck.getHand().get(i));
        }
        cardsLeft = cards.size();
    }

    public List<Card> getCards() {
        return cards;
    }

    public Integer getCardsLeft() {
        return cardsLeft;
    }

    public Deck shuffle () {
        Collections.shuffle(cards);
        return this;
    }

    // Identifies the Rank and Suit of the last card in the Deck, which in our game will be chosen as the Briscola.
    public Card peekLast () {
        return new Card(cards.get(cards.size()-1).getRank(), cards.get(cards.size()-1).getSuit());
    }

    /* A set number of cards is requested to be dealt.
    *  These cards are selected from the beginning of the deck, copied in the variable to be returned, and subsequently deleted from the deck.
    *  This will give the impression the cards have "moved", been dealt. */
    public List<Card> deal (Integer nOfCards)  {
        if (cards.size() < nOfCards) {
            System.err.println("Request to deal more cards then left in the cards");
            System.exit(-1);
            return cards;
        } else {
            List<Card> headSublist = cards.subList(0, nOfCards);
            List<Card> ret = new ArrayList<Card>(headSublist);
            headSublist.clear();
            cardsLeft = cards.size();
            return ret;
        }
    }

    @Override
    public String toString () {
        StringBuilder strBuilder = new StringBuilder();
        int i = 1;
        for (Card card : cards) {
            strBuilder.append(i++ + ". " + card + "\n");
        }
        return strBuilder.toString();
    }
}
