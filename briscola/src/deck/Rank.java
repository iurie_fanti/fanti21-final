package deck;

/* This enum represents the possible Ranks our deck allows. */
public enum Rank {
    ACE(10),
    TWO(1),
    THREE(9),
    FOUR(2),
    FIVE(3),
    SIX(4),
    SEVEN(5),
    JACK(6),
    KNIGHT(7),
    KING(8);

    /* The briscolaValue is a number assigned to each rank that defines its importance in the game.
    *  The cards worth the most points, or with the better ability to take a hand, are more valuable. */
    private final int briscolaValue;

    Rank(int briscolaValue) {
        this.briscolaValue = briscolaValue;
    }

    public int getBriscolaValue() {
        return briscolaValue;
    }
}