package deck;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/* This class represents a player's Hand during the course of the game. */
public class Hand {
    // The Logger we use to output messages onto the terminal.
    private static final Logger LOGGER = Logger.getLogger(Hand.class.getName());

    // The full list of all cards currently in the player's hand.
    private final List<Card> hand = new ArrayList<>();

    public Hand (List<Card> cards) {
        hand.addAll(cards);
    }

    public Hand shuffle(){
        Collections.shuffle(hand);
        return this;
    }

    public void addOne (Card toAdd) {
        hand.add(toAdd);
    }

    public void addAll (List<Card> toAdd) {
        hand.addAll(toAdd);
    }

    public boolean removeOne(Card toRemove) {
        if (!hand.remove(toRemove)) {
            try {
                for (int i = 0; i < hand.size(); i++) {
                    if (hand.get(i).getRank() == toRemove.getRank() && hand.get(i).getSuit() == toRemove.getSuit()) {
                        hand.remove(i);
                        return true;
                    }
                }
                return false;
            } catch (Exception e) {
                LOGGER.severe("ERROR while requested to remove card '" + toRemove + "'");
                System.exit(-1);
            }
        } else {
            return true;
        }
        return false;
    }

    public List<Card> getHand() {
        return hand;
    }

    @Override
    public String toString () {
        StringBuilder strBuilder = new StringBuilder("");
        int i = 1;
        for (Card card : hand) {
            strBuilder.append("\n" + i++ + ". " + card);
        }
        int idxOfLastComma = strBuilder.lastIndexOf(",");
        return (idxOfLastComma != -1) ?
                strBuilder.replace(idxOfLastComma, idxOfLastComma+1, "]").toString()
                :
                strBuilder.toString();
    }

}
