package deck;

/* This enum represents the possible Suits our deck allows. */
public enum Suit {
    CLUBS, CUPS, COINS, SWORDS;
}
