package briscola;
import main.GameMode;
import java.util.List;
import java.util.logging.Logger;

public class Engine {
    // The Logger we use to output messages onto the terminal.
    private static final Logger LOGGER = Logger.getLogger(Engine.class.getName());

    public int playNewGame(int starter, GameMode gameType, List<Integer> depthsOfSearch) {
        LOGGER.info("\nStarting new game of Briscola!\n");

        Game game = new Game(starter, gameType, depthsOfSearch);
        boolean isGameOver = false;

        // This loop is the main game function, repeating until the turns are over.
        do {
            LOGGER.info(game.toString()); // Outputs current status of the game.

            /* If we are able to have a nextPlayer, that means there are still turns left to play, and the game continues.
            *  Otherwise, that means the game is over, and there are no turns left. */
            int nextPlayer = game.getNextPlayer();
            if (nextPlayer == -1) isGameOver = true;
            else game.doNextTurn();
        } while (!isGameOver);

        // Of the two players that played, we check who won, and then output through the logger.
        int winner = Util.getGameWinner(game.getPlayers());

        if (winner == -1) {
            LOGGER.info("Game ended in a DRAW.");
        } else {
            LOGGER.info("Player " + game.getPlayers().get(winner).getName() + " won the game\n");

        }

        return winner;
    }

}
