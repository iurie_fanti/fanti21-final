package briscola;
import deck.*;
import java.util.ArrayList;
import java.util.List;

/* A class representing the current GameState. It represents the situation at the beginning of the AI's turn. */
class GameStateDetermination {
    public Hand board; // The cards that have already been played, that the AI has to beat (in 1-v-1 it's a single card)
    public Deck deck; // The game's deck. Practically, this will be a possibility based on the current unseen cards.
    public Card briscola; // The Briscola at the bottom of the deck.
    public Hand[] hands; // The AI's hand and the opponent's possible hand, based on the current unseen cards and hypothetical deck.
    public int[] points; // The current points held by both players.
    public Hand[] collectedCards; // Both player's collected cards so far into the game.
    public int turn; // What turn it is, from 1 to 20.
    public int nextPlayerIndex; // The next player after the AI.

    public GameStateDetermination(Hand board, Deck deck, Card briscola, Hand[] hands, int[] points, Hand[] collectedCards, int turn, int nextPlayerIndex) {
        this.board = board;
        this.deck = deck;
        this.briscola = briscola;
        this.hands = hands;
        this.points = points;
        this.collectedCards = collectedCards;
        this.turn = turn;
        this.nextPlayerIndex = nextPlayerIndex;
    }

    public GameStateDetermination(GameStateDetermination oldGameStateDetermination) {
        this.board = new Hand(new ArrayList<>(oldGameStateDetermination.board.getHand()));
        this.deck = new Deck(new Hand(new ArrayList<>(oldGameStateDetermination.deck.getCards())));
        this.briscola = oldGameStateDetermination.briscola;
        this.hands = new Hand[2];
        this.hands[0] = new Hand(new ArrayList<>(oldGameStateDetermination.hands[0].getHand()));
        this.hands[1] = new Hand(new ArrayList<>(oldGameStateDetermination.hands[1].getHand()));
        this.points = new int[2];
        this.points[0] = oldGameStateDetermination.points[0];
        this.points[1] = oldGameStateDetermination.points[1];
        this.collectedCards = new Hand[2];
        this.collectedCards[0] = new Hand(new ArrayList<>(oldGameStateDetermination.collectedCards[0].getHand()));
        this.collectedCards[1] = new Hand(new ArrayList<>(oldGameStateDetermination.collectedCards[1].getHand()));
        this.turn = oldGameStateDetermination.turn;
        this.nextPlayerIndex = oldGameStateDetermination.nextPlayerIndex;
    }

}




/* This Class contains most of the "processing" to be done on the GameState by the machine.
 *  It defines the AI's algorithm, determines winners for hands and matches, calculates points.
 *  All considerations and elaborations based on the ongoing match are made here. */
public class Util {
    // If the Rule-Based AI is first to play, it will pick the card of least value in its hand.
    public static int getIndexOfLessValueCard(Hand cards, Suit briscola) {
        // To search for the least-valued card in the hand, we start by comparing each of them to the highest possible value.
        int i, lessValueFound = 11, indexLessValueCard = -1;

        // We search the entire hand.
        for (i = 0; i < cards.getHand().size(); i++) {
            // IF: The card is not a briscola & has no points & has less value than the current known least-valued.
            if (cards.getHand().get(i).getSuit() != briscola
                    && cards.getHand().get(i).getRank().getBriscolaValue() <= 5
                    && lessValueFound > cards.getHand().get(i).getRank().getBriscolaValue()) {
                // THEN: Our newest known least-valued card is this one.
                lessValueFound = cards.getHand().get(i).getRank().getBriscolaValue();
                // We save the index of the found card to return.
                indexLessValueCard = i;
            }
        }

        // If our search replaced the dummy initial value, it is the least valuable card in the hand.
        if (indexLessValueCard != -1) return indexLessValueCard;


        // If not, we go on a new search, for a slightly more valuable card.
        for (i = 0; i < cards.getHand().size(); i++) {
            // IF: The card is not a briscola & is not an Ace or a Three (worth 11 or 10 points) & has less value than the current known least-valued.
            if (cards.getHand().get(i).getSuit() != briscola
                    && cards.getHand().get(i).getRank().getBriscolaValue() <= 8
                    && lessValueFound > cards.getHand().get(i).getRank().getBriscolaValue()) {
                // THEN: Our newest known least-valued card is this one.
                lessValueFound = cards.getHand().get(i).getRank().getBriscolaValue();
                // We save the index of the found card to return.
                indexLessValueCard = i;
            }
        }

        // If our search replaced the dummy initial value, it is the least valuable card in the hand.
        if (indexLessValueCard != -1) return indexLessValueCard;


        // If not, we go on a new search, for a slightly more valuable card.
        for (i = 0; i < cards.getHand().size(); i++) {
            // IF: The card is not an Ace or a Three (worth 11 or 10 points) & has less value than the current known least-valued.
            if (cards.getHand().get(i).getRank().getBriscolaValue() <= 8
                    && lessValueFound > cards.getHand().get(i).getRank().getBriscolaValue()) {
                // THEN: Our newest known least-valued card is this one.
                lessValueFound = cards.getHand().get(i).getRank().getBriscolaValue();
                // We save the index of the found card to return.
                indexLessValueCard = i;
            }
        }

        // If our search replaced the dummy initial value, it is the least valuable card in the hand.
        if (indexLessValueCard != -1) return indexLessValueCard;


        // If not, we go on a new search, for a slightly more valuable card.
        for (i = 0; i < cards.getHand().size(); i++) {
            // IF: The card has less value than the current known least-valued.
            if (lessValueFound > cards.getHand().get(i).getRank().getBriscolaValue()) {
                // THEN: Our newest known least-valued card is this one.
                lessValueFound = cards.getHand().get(i).getRank().getBriscolaValue();
                // We save the index of the found card to return.
                indexLessValueCard = i;
            }
        }

        // At this point we must have a card to play, since even the most valuable card of the game is lesser than our dummy initial value.
        return indexLessValueCard;
    }

    // If the Rule-Based AI is second to play, it will pick the least-valuable winning card if it has one in its hand, alternatively, the card of least value.
    public static int getIndexOfLessValueWinningCardOtherwiseLessValue(Hand cards, Suit briscola, Card tableCard) {
        // List of all possible winning cards found in the search.
        List<Card> winningCards = new ArrayList<>();
        // Index associated with the found winning cards.
        List<Integer> winningCardsIndices = new ArrayList<>();

        // Searches the entire hand for winning cards.
        for (int i = 0; i < cards.getHand().size(); i++) {
            // Creates a Hand comprised of the other player's card, and the card we are currently evaluating.
            Hand newTable = new Hand(new ArrayList<>());
            newTable.addOne(tableCard);
            newTable.addOne(cards.getHand().get(i));

            // If the hand is won, and grants us any amount of points, we consider this a winning card.
            if (getHandWinner(newTable, briscola) == 1 && Util.calculatePoints(newTable) > 0) {
                winningCards.add(cards.getHand().get(i));
                winningCardsIndices.add(i);
            }
        }

        // Searches our found winning cards, if we have any.
        if (winningCards.size() > 1) {
            // Plays the least valuable one, in case there are multiple.
            return winningCardsIndices.get(getIndexOfLessValueCard(new Hand(winningCards), briscola));
        } else if (winningCards.size() == 1) {
            // If there is only one winning card, that is the one played.
            return winningCardsIndices.get(0);
        } else {
            // If we found no winning cards, we fall back to playing the least valuable in our hand.
            return getIndexOfLessValueCard(cards, briscola);
        }
    }

    // The MinMax-AI will play the best card based on a heuristic evaluation of all possible gamestates, based on the currently unseen hands.
    public static Card monteCarloApproximation(int search_depth, int numberOfRandomDeals,
                                               Hand myHand, Card briscola, Hand board, Hand unseenCards,
                                               Hand myCollectedCards, Hand opponentCollectedCards,
                                               int turn, int opponentNOfCard) {

        // Number of possible ways of arranging the unseen cards.
        long totalUnseenCardsPermutations = factorial(unseenCards.getHand().size());

        // Variable to hold possible "deals" explored so far.
        // We store their serialization so that they can be compared with other decks (that will also have been serialized).
        List<int[]> dealsSearchedSerialization = new ArrayList<>();

        // Variable to hold randomized Decks.
        List<Deck> randomizedDecks = new ArrayList<>();


        int dealsSearchedCounter = 0;

        /* We keep searching until we have explored all possible permutations of unseen cards,
           or until we exceed the number of possible random Deals. */
        while (dealsSearchedCounter < numberOfRandomDeals && dealsSearchedCounter != totalUnseenCardsPermutations) {
            Deck randomDeck;
            int[] randomDeckSerialization;

            // We obtain a random Deck that we have not already stored.
            do {
                randomDeck = new Deck(unseenCards.shuffle());
                randomDeckSerialization = serializeCards(randomDeck);
            } while (alreadyExist(dealsSearchedSerialization, randomDeckSerialization));

            // If we find a new random deck of unseen, we add it to our stored serialized decks.
            dealsSearchedSerialization.add(randomDeckSerialization);

            // We add the Briscola to our random Deck of unseen cards.
            randomDeck.getCards().add(briscola);

            // We add our random Deck of unseen cards to our stored decks.
            randomizedDecks.add(randomDeck);

            dealsSearchedCounter++;
        }

        // At this point, we should have obtained a number of possible decks from the cards the AI has not yet seen.


        // Hands will be used to store the AI's hand, and later compare them to the possible opponent's hand.
        Hand[] hands = new Hand[2];
        hands[0] = new Hand(new ArrayList<>(myHand.getHand()));

        // The current standing of the game, AI's points and opponent's points.
        int[] points = new int[]{calculatePoints(myCollectedCards), calculatePoints(opponentCollectedCards)};

        // Both player's collected cards so far into the game.
        Hand[] collectedCards = new Hand[2];
        collectedCards[0] = new Hand(new ArrayList<>(myCollectedCards.getHand()));
        collectedCards[1] = new Hand(new ArrayList<>(opponentCollectedCards.getHand()));

        // The possible "actions" of this turn, meaning the possible cards that can be played.
        List<Card> legalActions = getActions(myHand);

        // The "confidence" the AI has in a card being the best choice. The highest confidence is played.
        int[] cardConfidenceValues = new int[myHand.getHand().size()];


        // ???
        for (Deck randomizedDeck : randomizedDecks) {
            // Instances a possible opponent hand, to be considered by the AI.
            hands[1] = new Hand(new ArrayList<>(randomizedDeck.deal(opponentNOfCard)));

            // The hypothetical state of the game based on the possible deck and hands.
            GameStateDetermination initialGSD = new GameStateDetermination(new Hand(new ArrayList<>(board.getHand())), randomizedDeck, briscola, hands, points, collectedCards, turn, 0);

            // The best card to play, based on our current hypothetical game-state.
            int bestCardIndexCurrentGDS = 0;

            double resultValue = Double.NEGATIVE_INFINITY;

            // Evaluates each possible action of this turn (each possible card to play).
            for (int j = 0; j < legalActions.size(); j++) {
                // Assigns a value to the action of playing the current card. The higher, the better.
                double value = heuristicMinMaxAlphaBeta(getResult(initialGSD, legalActions.get(j)), Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, search_depth);

                // If this is the best value we have found yet, we save the card's index.
                if (value > resultValue) {
                    bestCardIndexCurrentGDS = j;
                    resultValue = value;
                }

            }

            /* We have evaluated all possible actions of this hypothetical game state.
            *  The best card to play for this gamestate will increase our confidence in that card by 1.
            *  After we evaluate all actions of all the random decks' gamestates, each card will have a certain level of confidence,
            *  based on how well it does overall throughout the hypothetical scenarios. */
            cardConfidenceValues[bestCardIndexCurrentGDS] += 1;
        }

        // We find the card of highest confidence within our hand.
        int bestCardIndex = 0;
        for (int i = 0; i < myHand.getHand().size(); i++) {
            if (cardConfidenceValues[i] > cardConfidenceValues[bestCardIndex]) {
                bestCardIndex = i;
            }

        }

        // We play the card of highest confidence within our hand.
        return myHand.getHand().get(bestCardIndex);
    }

    // We determine the value of a certain gamestate, by exploring deeper into the progression of the game from that point.
    public static double heuristicMinMaxAlphaBeta(GameStateDetermination gsd, double alpha, double beta, int depth) {
        // If the AI goes second, our evaluation will aim to maximise results, otherwise it will aim to minimise losses.
        double value = (gsd.nextPlayerIndex == 0 ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY);

        // If this is the last turn, or we have explored as far as our depth allows.
        if (gsd.turn == 19 || depth <= 0) {
            // We return the gap between the AI's points and the opponent's.
            return (gsd.points[0] - gsd.points[1]);
        }

        // We will evaluate every card currently in the AI's hand.
        for (Card action : getActions(gsd.hands[gsd.nextPlayerIndex])) {

            // If the AI goes second, the best choice is the one that maximises points.
            if (gsd.nextPlayerIndex == 0) {
                // We invoke a recursive function, to evaluate the result of playing this card.
                value = Math.max(value, heuristicMinMaxAlphaBeta(getResult(gsd, action), alpha, beta, depth - 1));

                // If the value of this move reaches infinity, that is obviously the best move.
                if (value >= beta) { return value; }

                // If the found value is higher than the previous highest, we consider this as the best move.
                alpha = Math.max(alpha, value);

            // If the AI goes first, the best choice is the one that minimises the opponent's points.
            } else {
                // We invoke a recursive function, to evaluate the result of playing this card.
                value = Math.min(value, heuristicMinMaxAlphaBeta(getResult(gsd, action), alpha, beta, depth - 1));

                // If the value of this move is love than negative infinity, that is obviously the best move.
                if (value <= alpha) { return value; }

                // If the found value is lower than the previous lowest, we consider this as the best move.
                beta = Math.min(beta, value);
            }
        }

        return value;
    }

    // We have a gamestate, and a card we mean to play. This method returns the resulting gamestate after the player's action.
    public static GameStateDetermination getResult(GameStateDetermination gsd, Card cardPlayed) {
        // The GameState we will be considering.
        GameStateDetermination resultGSD = new GameStateDetermination(gsd);

        // Exits if the card played is not in the player's hand.
        if (!resultGSD.hands[gsd.nextPlayerIndex].removeOne(cardPlayed)) { System.exit(-1); }

        // Plays the selected card, in order to consider a gamestate where the AI has played.
        resultGSD.board.addOne(cardPlayed);

        // If both players have played their card.
        if (resultGSD.board.getHand().size() == 2) {
            // Returns the turn's winning player.
            int winner = getHandWinner(resultGSD.board, resultGSD.briscola.getSuit());

            // Picks the next player to start the next turn, since the winner is the first to go.
            resultGSD.nextPlayerIndex = (winner != 1 ? toggle(resultGSD.nextPlayerIndex) : resultGSD.nextPlayerIndex);

            // The winner adds to their collection the cards on the board.
            resultGSD.collectedCards[resultGSD.nextPlayerIndex].addAll(resultGSD.board.getHand());

            // The board is cleared.
            resultGSD.board.getHand().clear();

            // The winner's score is updated, based on the new cards collected.
            resultGSD.points[resultGSD.nextPlayerIndex] = calculatePoints(resultGSD.collectedCards[resultGSD.nextPlayerIndex]);

            // It is now the next turn.
            resultGSD.turn++;

            // If we are not in the last 3 turns, the players can still draw from the deck.
            if (resultGSD.turn < 17) {
                // The previous turn's winner is the first to draw.
                if (resultGSD.nextPlayerIndex == 0) {
                    resultGSD.hands[0].addOne(resultGSD.deck.deal(1).get(0));
                    resultGSD.hands[1].addOne(resultGSD.deck.deal(1).get(0));
                } else {
                    resultGSD.hands[1].addOne(resultGSD.deck.deal(1).get(0));
                    resultGSD.hands[0].addOne(resultGSD.deck.deal(1).get(0));
                }
            }

        // If only one player has played.
        } else {
            // The turn is passed over to the next player.
            resultGSD.nextPlayerIndex = toggle(resultGSD.nextPlayerIndex);
        }

        // Returns the gamestate at the end of the AI's move.
        return resultGSD;
    }

    // Returns the possible "actions" of this turn. In our case: the possible cards that can be played.
    public static List<Card> getActions(Hand cards) {
        return cards.getHand();
    }

    // Determines the winner of a turn.
    public static Integer getHandWinner(Hand board, Suit briscolaSuit) {

        // Obtains the card, suit, and rank, for the first card played.
        Card c1 = board.getHand().get(0);
        Suit s1 = c1.getSuit();
        Rank r1 = c1.getRank();

        // Obtains the card, suit, and rank, for the second card played.
        Card c2 = board.getHand().get(1);
        Suit s2 = c2.getSuit();
        Rank r2 = c2.getRank();

        // If one card is a Briscola and the other is not, that card's player wins.
        if (s1 == briscolaSuit && s1 != s2) return 0;
        if (s2 == briscolaSuit && s1 != s2) return 1;

        // If both cards are of the same suit, the highest valued card wins.
        if (s1 == s2) return r1.getBriscolaValue() > r2.getBriscolaValue() ? 0 : 1;

        // If there is no Briscola, and the cards have different suits, the first card's player wins.
        return 0;
    }

    // Determines the winner of the match.
    public static int getGameWinner(List<Player> players) {
        int winnerPlayerIndex = 0, pointsMax = 0;

        // Examines both players.
        for (int i = 0; i < players.size(); i++) {
            // Calculates the points within all taken cards.
            int points = calculatePoints(players.get(i).getCardsCollected());

            // If these points surpass the previous highest-points, that player wins.
            if (points > pointsMax) {
                pointsMax = points;
                winnerPlayerIndex = i;

            // If the first player and the second player have the same points, it's a draw.
            } else if (points == pointsMax) {
                winnerPlayerIndex = -1;
            }
        }

        return winnerPlayerIndex;
    }

    // Determines the points taken by a player.
    public static Integer calculatePoints (Hand cards) {
        int counter = 0;

        // Goes through every taken card, and adds points for every valuable card collected.
        for (Card card : cards.getHand()) {
            switch (card.getRank()) {
                case ACE:
                    counter += 11;
                    break;
                case THREE:
                    counter += 10;
                    break;
                case KING:
                    counter += 4;
                    break;
                case KNIGHT:
                    counter += 3;
                    break;
                case JACK:
                    counter += 2;
                    break;
                default:
                    break;
            }
        }

        return counter;
    }

    // By "serializing" we mean assigning a specific numeric value to each card, based on its Rank and Suit.
    public static int[] serializeCards (Deck deck) {
        // Creates an array for card Codes, with the same size as the number of cards in the deck.
        int[] cardCodes = new int[deck.getCards().size()];

        // Will go through every card in the deck, and assign to each a code.
        for (int i = 0; i < deck.getCards().size(); i++) {
            // Adds the card's value, based on its Rank.
            cardCodes[i] = deck.getCards().get(i).getRank().getBriscolaValue(); // Possible results: [1 to 10]

            // An "offset" value is added, based on its Suit.
            switch (deck.getCards().get(i).getSuit()) {
                /* case CLUBS:
                    we add 0;               Possible results: [1 to 10] */
                case CUPS:
                    cardCodes[i] += 10;  // Possible results: [11 to 20]
                    break;
                case SWORDS:
                    cardCodes[i] += 20;  // Possible results: [21 to 30]
                    break;
                case COINS:
                    cardCodes[i] += 30;  // Possible results: [31 to 40]
            }
        }

        // The codes we obtain can uniquely identify every possible card in the Game.
        return cardCodes;
    }

    // Checks if a certain Deck is already stored in a certain list of Decks.
    public static boolean alreadyExist (List<int[]> oldDecks, int[] newDeck) {
        // Searches the entire oldDecks list.
        for (int[] oldDeck : oldDecks) {
            // Begins with the assumption newDeck exists within this specific oldDeck.
            boolean sameDecks = true;

            // Searches a specific oldDeck.
            for (int j = 0; j < oldDeck.length; j++) {
                // If there is a difference between the oldDeck and newDeck, they are not the same.
                if (oldDeck[j] != newDeck[j]) {
                    sameDecks = false;
                }
            }

            // If an oldDeck has been searched and the variable has not changed, it means it found a match.
            if (sameDecks) {
                return true;
            }
        }

        // If we reach here it means no sameDeck has been found, therefore there is no match.
        return false;
    }

    // Returns the factorial of a number. Meaning, the number of possible ways to arrange n items.
    public static long factorial (int n) {
        if (n >= 19) return Long.MAX_VALUE;

        if (n == 0 || n == 1) return 1;
        else return factorial(n - 1) * n;
    }

    // Switches from one player to the other.
    public static int toggle(int actualPlayerID) {
        return (actualPlayerID == 0 ? 1 : 0);
    }
}