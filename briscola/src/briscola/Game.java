package briscola;
import deck.*;
import main.GameMode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Game {
    // The Logger we use to output messages onto the terminal.
    private static final Logger LOGGER = Logger.getLogger(Game.class.getName());

    private final Deck deck;
    private final Hand board;
    private final Card briscola;
    private final List<Player> players = new ArrayList<Player>();

    // Turns that have been played so far
    private int turns;
    // Whose turn is up next after the current player. Can be used to determine if the game is over.
    private Integer nextPlayer;


    // Defines the Game's entities, meaning the card setup, and the participating players.
    public Game(int starter, GameMode mod, List<Integer> searchParameters) {
        turns = 0;
        deck = new Deck();
        nextPlayer = starter;
        briscola = deck.shuffle().peekLast(); // The last card in the deck is chosen as the Briscola suit.
        board = new Hand(new ArrayList<>());

        // The players added into the game vary depending on the GameMode chosen.
        switch (mod) {
            default:
            case HUMAN__VS__AI_RULE:
                players.add(new AI(0, "AI-Rule", new Hand(deck.deal(3)), -1, false, -1));
                players.add(new Human(1, "Human", new Hand(deck.deal(3))));
                break;

            case HUMAN__VS__AI_MINMAX:
                players.add(new AI(0, "AI-MinMax", new Hand(deck.deal(3)), searchParameters.get(0), false, searchParameters.get(1)));
                players.add(new Human(1, "Human", new Hand(deck.deal(3))));
                break;

            case AI_MINMAX__VS__AI_RULE:
                players.add(new AI(0, "AI-MinMax", new Hand(deck.deal(3)), searchParameters.get(0), false, searchParameters.get(1)));
                players.add(new AI(1, "AI-Rule", new Hand(deck.deal(3)), -1, false, -1));
                break;

            case AI_MINMAX__VS__AI_RANDOM:
                players.add(new AI(0, "AI-MinMax", new Hand(deck.deal(3)), searchParameters.get(0), false, searchParameters.get(1)));
                players.add(new AI(1, "AI-Random", new Hand(deck.deal(3)), -1, true, -1));
                break;
        }

    }

    public List<Player> getPlayers() {
        return players;
    }

    // There is a nextPlayer only if the game is continuing, and since 2-Player briscola has at most 20 turns, that is the limit.
    public int getNextPlayer() {
        if (turns < 20) return nextPlayer;
        else return -1;
    }

    // The method invoked for every turn in the game. It invokes the player's method that allows them to make their choice.
    public void doNextTurn() {
        LOGGER.info("To play: " + players.get(nextPlayer).getName() + "\n");

        // The card that will be played this turn.
        Card nextCard;

        // The turn is played differently if the player is the AI rather than a Human.
        if (players.get(nextPlayer) instanceof AI) {
            // The AI plays with imperfect information, so it can only "see" its own cards.
            Hand unseenCards = new Hand(new ArrayList<>(deck.getCards())); // Cannot see cards currently in the deck.
            unseenCards.addAll(players.get(Util.toggle(nextPlayer)).getCards().getHand()); // Cannot see cards in the opponent's hand.
            unseenCards.removeOne(briscola); // The Briscola at the bottom of the deck is the exception.

            nextCard = players.get(nextPlayer).play(
                    board,
                    briscola,
                    unseenCards,
                    turns,
                    players.get(Util.toggle(nextPlayer)).getCardsCollected(),
                    players.get(Util.toggle(nextPlayer)).getCards().getHand().size());

        } else {
            nextCard = players.get(nextPlayer).play();
        }

        /* At this point nextCard has been chosen by the player.
        *  It is removed from the player's hand, and added to the board. */
        players.get(nextPlayer).removeCardFromHand(nextCard);
        board.addOne(nextCard);

        // The board is evaluated. If both have played, then a winner is chosen and the board is cleared.
        collectAndDeal();
    }


    /* Evaluates the current board.
    *  If both players have set their card, then a winner is chosen, and the turn's result is printed.
    *  Otherwise, nothing is done, besides designating the next player. */
    public void collectAndDeal() {
        // If the board has a size of two, that means both players have set their card.
        if (board.getHand().size() == 2) {
            // Prints the board, meaning the cards played.
            LOGGER.info("Cards played: " + board.getHand());

            // Determines the winner of this turn, based on the values of the cards.
            int handWinner = Util.getHandWinner(board, briscola.getSuit());

            /* The current "nextPlayer" is the one who played last.
            *  With this IF we toggle to the first player again, so they may start at the next turn.
            *  If nothing is done, it means the second player won, and it will be them starting. */
            if (handWinner == 0) {
                nextPlayer = Util.toggle(nextPlayer);
            }

            LOGGER.info(players.get(nextPlayer).getName() + " won this turn.\n");

            // The cards on the board are added to the winner's points.
            players.get(nextPlayer).collectCards(board);

            // The board is cleared, and the next turn can start fresh.
            board.getHand().clear();

            // If there are more than 3 turns left, it means there are still enough cards for the players to draw.
            if ((20 - turns) > 3) {  // i.e. turns >= 16
                // Both players are dealt a hand from the deck. Winner first.
                players.get(nextPlayer).addNewCardToHand(deck.deal(1).get(0));
                players.get(Util.toggle(nextPlayer)).addNewCardToHand(deck.deal(1).get(0));
            }

            // Current turn is increased.
            turns++;

        } else {
            // If we reach here, it means only one player has set their card and the turn is still ongoing, so we just switch to the next player.
            nextPlayer = Util.toggle(nextPlayer);
        }
    }


    // The string representing the current status of the game, printed by the Engine at the start of each turn.
    @Override
    public String toString() {
        return players.get(0).getName() + " (" + Util.calculatePoints(players.get(0).getCardsCollected()) + " pts)" + players.get(0).getCards() + "\n" +
                "\n" +
                "Board: " + board.getHand() + "   " +
                "(Turns left: " + (20 - turns) + "  /  Briscola: " + briscola + ")\t\t\t" +
                "\n\n" +
                players.get(1).getName() + " (" + Util.calculatePoints(players.get(1).getCardsCollected()) + " pts) " + players.get(1).getCards() +
                "\n-------------------------------------------------------------------------------------------------------------------------------------------------\n";
    }
}
