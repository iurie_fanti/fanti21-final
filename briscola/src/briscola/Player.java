package briscola;
import deck.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Logger;

/* The abstract class upon which both AI and Human players are defined. */
abstract class Player {
    int ID;
    String name;
    Hand cards; // The cards currently in their hand.
    Hand cardsCollected; // The cards they won during the game.

    // At the beginning of the game, the Player will be assigned an ID, a name, an initial hand, and they will have no collected cards.
    public Player (int ID, String name, Hand initialHand) {
        this.ID = ID;
        this.name = name;
        this.cards = initialHand;
        this.cardsCollected = new Hand(new ArrayList<>());
    }

    abstract public Card play();

    abstract public Card play(Hand board, Card briscola, Hand unknownCards, int turn, Hand opponentCardsCollected, int opponentNOfCard);

    public String getName() {
        return name;
    }

    public Hand getCards() {
        return cards;
    }

    public Hand getCardsCollected() {
        return cardsCollected;
    }

    // The cards on the board will be added to the Player's collection of winnings.
    public void collectCards(Hand newCardsToCollect) {
        cardsCollected.addAll(new ArrayList<>(newCardsToCollect.getHand()));
    }

    // Invoked during the draw phase at the beginning of a turn.
    public void addNewCardToHand(Card newCard) {
        cards.addOne(newCard);
    }

    // Invoked when a card is played.
    public void removeCardFromHand(Card toRemove){
        cards.removeOne(toRemove);
    }
}


/* Class representing and handling an AI player. */
class AI extends Player {
    int depth; // Maximum depth for the search algorithm.
    boolean randomPlay; // Flags whether it's an "actual" AI, or just a dud playing randomly.
    int numberOfRandomDeals;


    public AI (int ID, String name, Hand initialHand, int depth, boolean randomPlay, int decks) {
        super(ID, name, initialHand);
        this.depth = depth;
        this.randomPlay = randomPlay;
        this.numberOfRandomDeals = decks;
    }

    // Picks a random card from its hand.
    public Card chooseRandom() {
        return cards.getHand().get(new Random().nextInt(cards.getHand().size()));
    }

    @Override
    public Card play(Hand board, Card briscola, Hand unknownCards, int turn, Hand opponentCardsCollected, int opponentNOfCard) {
        // At the 19th turn, only one card will be left in the hand, so there is no choice to make.
        if (turn == 19) return cards.getHand().get(0);

        // Predefined card choice in case the AI is set to Random.
        if (randomPlay) return chooseRandom();

        // Rule-Based decision.
        if (depth == -1) {

            // AI plays first. It will pick the card of least value in its hand.
            if (board.getHand().size() == 0) {
                return cards.getHand().get(Util.getIndexOfLessValueCard(cards, briscola.getSuit()));

            // AI plays second. It will pick the least-valuable winning card if it has one in its hand, alternatively, the card of least value.
            } else {
                return cards.getHand().get(Util.getIndexOfLessValueWinningCardOtherwiseLessValue(cards, briscola.getSuit(), board.getHand().get(0)));
            }

        // Search-Based decision.
        } else {
            return Util.monteCarloApproximation(
                    depth,
                    numberOfRandomDeals,
                    cards,
                    briscola,
                    board,
                    unknownCards,
                    cardsCollected,
                    opponentCardsCollected,
                    turn,
                    opponentNOfCard);
        }
    }

    // Method must be implemented to comply with the Abstract Class, but it will not be used.
    @Override
    public Card play() {
        return null;
    }
}


/* Class representing and handling a Human player. */
class Human extends Player {
    // Logger used to communicate with the user if they need correcting during their turn.
    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    // Constructor is the same basic one from the Abstract class.
    public Human(int ID, String name, Hand initialHand) {
        super(ID, name, initialHand);
    }

    /* The user is prompted to select a card to play. */
    @Override
    public Card play() {
        Scanner sc = new Scanner(System.in);
        Card cardSelected;

        /* Prompts input until a valid card is selected from the hand. */
        while (true) {
            int inputIDX = sc.nextInt();

            // The Player has at least 1 card to play, and up to either 2 or 3, outside of the current bounds the input is invalid.
            if (inputIDX < 1 || inputIDX > cards.getHand().size()) {
                LOGGER.severe("Wrong card number entered, please try again (enter 1,2,3)");
                continue;
            } else {
                cardSelected = cards.getHand().get(inputIDX - 1);
                break;
            }
        }

        // The chosen card is passed onto the Game, where it will be removed from the hand and added to the board.
        return cardSelected;
    }

    // Method must be implemented to comply with the Abstract Class, but it will not be used.
    @Override
    public Card play(Hand board, Card briscola, Hand unknownCards, int turn, Hand opponentCardsCollected, int opponentNOfCard) {
        return null;
    }
}
